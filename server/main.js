import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import axios from 'axios';
import striptags from 'striptags';
import _ from 'lodash';

const TokenCache = new Mongo.Collection('tokens');

const API_URL = 'http://193.124.114.46:3001';
const API_METHODS = {
	'register': '/users',
	'login': '/sessions/create',
	'profile': '/api/protected/user-info',
	'userList': '/api/protected/users/list',
	'txList': '/api/protected/transactions',
	'pay': '/api/protected/transactions',
};

function getRequestUrl(action) {
	return API_URL + (API_METHODS[action] || 'none');
}

async function getToken() {
	const val = await TokenCache.find({}, { sort: { created: -1 }, limit: 1 }).fetch();
	console.log(val);
	return val.token || null;
}

Meteor.methods({
    'api.auth'(args) {
		return axios.post(getRequestUrl('login'), {
			email: args.login,
			password: args.passwd,
		})
		.then((res) => {
			return res.data.id_token || null;
		})
		.catch((err) => {
			console.log(err);
			return null;
		});
	},

	'api.register'(args) {
		return axios.post(getRequestUrl('register'), {
			email: args.mail,
			username: args.user,
			password: args.passwd,
		})
		.then((res) => {
			return res.data.id_token || null;
		})
		.catch((err) => {
			console.log(err);
			return false;
		});
	},

	'api.getUserData'(token) {
		if (!token) {
			return false;
		}

		const config = {
        	headers: {'Authorization': "bearer " + token},
   		};

		return axios.get(getRequestUrl('profile'), config)
			.then((res) => {
				return res.data.user_info_token;
			})
			.catch((err) => {
				console.log(err);
				return null;
			});
	},

	'api.listUsers'(args) {
		if (!args.token) {
			return false;
		}

		const config = {
        	headers: {'Authorization': "bearer " + args.token},
   		};

		return axios.post(getRequestUrl('userList'), { filter: args.filter }, config)
			.then((res) => {
				return res.data;
			})
			.catch((err) => {
				console.log(err);
				return null;
			});
	},

	'api.newTransaction'(args) {
		if (!args.token) {
			return false;
		}

		const config = {
        	headers: {'Authorization': "bearer " + args.token},
   		};

		return axios.post(getRequestUrl('pay'), { amount: args.amount, name: args.user }, config)
			.then((res) => {
				return res.data.trans_token;
			})
			.catch((err) => {
				console.log(err);
				return null;
			});
	},


	'api.listTransactions'(args) {
		if (!args.token) {
			return false;
		}

		const config = {
        	headers: {'Authorization': "bearer " + args.token},
   		};

		return axios.get(getRequestUrl('txList'), config)
			.then((res) => {
				return res.data.trans_token;
			})
			.catch((err) => {
				console.log(err);
				return null;
			});
	},
});


Meteor.startup(() => {
    console.log('App started');
    TokenCache.remove(); // clean tokens from old sessions
});
