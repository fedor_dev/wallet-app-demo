import React from 'react';
import Autocomplete from 'react-autocomplete';
import { bindActionCreators } from 'redux';  
import { connect } from 'react-redux';

import AuthPage from './authPage.js';
import UserInfo from './userInfo.js';
import TxList from './transactionsList.js';

import widgetActionCreators from './actions/widget-actions';

class App extends React.Component {
    componentDidMount() {
        const { widgetActions } = this.props;
        widgetActions.getProfile();
        setTimeout(widgetActions.getTransactions, 1200);
    }

    handleForm(e) {
        e.preventDefault();
    }

    onLogout(e) {
        e.preventDefault();
        const { widgetActions } = this.props;
        widgetActions.logout();
    }

    newPayment(e) {
        const { widgetActions, profile, suggestVal } = this.props;
        const amount = parseInt(this.refs.amount.value, 10);

        if (!suggestVal || suggestVal.length < 3) {
            return widgetActions.showError('Please select recepient user');
        }

        if (!amount || amount < 1) {
            return widgetActions.showError('Please use correct amount value');
        }

        if (amount > parseInt(profile.balance, 10)) {
            return widgetActions.showError('Insufficient funds on your balance');
        }

        setTimeout(widgetActions.getTransactions, 1000);
        return widgetActions.newTransaction(suggestVal, amount);
    }

    reloadTx (e) {
        e.preventDefault();
        const { widgetActions } = this.props;
        widgetActions.getTransactions();
    }
  
    render() {
        const { profile, transactions, widgetActions, suggestVal, suggestList, payError } = this.props;
        let data;
        if (!profile.name) {
            data = <AuthPage onLogin={widgetActions.login} onRegister={widgetActions.register} />
        } else {
            data = (
                <div>
                    <UserInfo data={profile} onLogout={this.onLogout.bind(this)} />
                    
                    <div class="panel panel-default user_panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Make payment</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="dropdown">
                                    <Autocomplete
                                        value={ suggestVal }
                                        inputProps={{ id: 'user-autocomplete' }}
                                        wrapperStyle={{ position: 'relative', display: 'inline-block' }}
                                        items={ suggestList }
                                        getItemValue={ item => item.name }
                                        shouldItemRender={ () => true }
                                        onChange={(event, value) => widgetActions.suggestOnChange(value)}
                                        onSelect={ value => widgetActions.suggestSelected(value) }
                                        renderMenu={ children => (
                                            <div className = "dropdown-content">
                                            { children }
                                            </div>
                                        )}
                                        renderItem={ (item, isHighlighted) => (
                                            <div
                                            key={ item.id } >
                                            { item.name }
                                            </div>
                                        )}
                                    />
                                    </div>
                                </div>
                                <div class="col-md-4"><input type="text" ref="amount" placeholder="Amount" /></div>
                                <div class="col-md-2"><button onClick={this.newPayment.bind(this)}>Pay</button></div>
                            </div>
                            <b>{ payError }</b>
                        </div>
                    </div>

                    <button class="btn btn-primary vertical_spacing" onClick={this.reloadTx.bind(this)}>
                        Reload transactions list
                    </button>

                    <div class="panel panel-default user_panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Last transactions</h3>
                        </div>
                        <div class="panel-body">
                            <TxList transactions={transactions ? transactions : []} />
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div class="container">
                <div class="row">
                    {data}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {  
  return {
    suggestList: state.suggestions,
    suggestVal: state.suggestVal,
    payError: state.payError,
    profile: state.profile,
    transactions: state.transactions,
    showLoader: state.loader,
  }
}

function mapDispatchToProps(dispatch) {  
  return {
    widgetActions: bindActionCreators(widgetActionCreators, dispatch),
  }
}

export default connect(  
  mapStateToProps,
  mapDispatchToProps
)(App);
