import PropTypes from 'prop-types';
import React from 'react';

const UserInfo = (props) => (
    <div class="panel panel-default user_panel">
        <div class="panel-heading">
            <h3 class="panel-title">Your wallet</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-5">Name: {props.data.name}</div>
                <div class="col-md-5">Email: {props.data.email}</div>
                <div class="col-md-2"><a onClick={props.onLogout}>Logout <i class="fas fa-sign-out-alt"></i></a></div>
            </div>
            <div class="row">
                <div class="col-md-6"><h3>Balance: {props.data.balance}</h3></div>
            </div>
        </div>
    </div>
);

UserInfo.displayName = 'Your wallet';
UserInfo.propTypes = {
    onLogout: PropTypes.func,
};

export default UserInfo;
