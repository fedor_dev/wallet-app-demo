import PropTypes from 'prop-types';
import React from 'react';

const TxList = (props) => (
    <div class="table-container">
        <table class="table-users table" border="0">
            <thead>
                <tr><th>Time</th><th>Amount</th><th>User</th><th>Type</th><th>Balance</th></tr>
            </thead>
            <tbody>
            { props.transactions.length < 1 ? 'There is no transactions found' : '' }
            { props.transactions.reverse().map(w => (
                <tr>
                    <td align="center">
                        <small class="text-muted">{ w.date }</small>
                    </td>
                    <td width="20">
                        {Math.abs(w.amount)}  
                    </td>
                    <td>
                        {w.username } <i class="fas fa-user"></i>
                    </td>
                    <td align="center">
                        <small class="text-muted">{ w.amount > 0 ? 'Accepted' : 'Paid' }</small>
                    </td>
                    <td width="20">
                        {w.balance}  
                    </td>
                </tr>
            ))}
      </tbody>
    </table>
  </div>
);

TxList.displayName = 'Last transactions';

TxList.propTypes = {
  transactions: PropTypes.array,
};

export default TxList;
