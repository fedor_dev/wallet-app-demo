function getProfile() {  
    return async function(dispatch) {
        console.log('called getProfile');

        const token = localStorage.getItem('auth_token');

        let data = {};
        if (token) {
            data = await Meteor.callPromise('api.getUserData', token);
        }
        console.log(data);
        return dispatch({
            type: 'SET_INFO',
            payload: data
        });
    }
}

function login(data) {  
    return async function(dispatch) {
        if (!data.login || !data.passwd) {
            return false;
        }
        const token = await Meteor.callPromise('api.auth', data);
        console.log("token:", token);
        localStorage.setItem('auth_token', token);

        const profile = await Meteor.callPromise('api.getUserData', token);
        console.log("profile", profile);
        return dispatch({
            type: 'SET_INFO',
            payload: profile
        });
    }
}

function logout(data) {  
    return async function(dispatch) {
        localStorage.clear();
        return dispatch({
            type: 'SET_INFO',
            payload: {}
        });
    }
}

function register(data) {  
    return async function(dispatch) {
        console.log('called register');
        if (!data.user || !data.mail || !data.passwd) {
            return false;
        }

        const token = await Meteor.callPromise('api.register', data);
        if (!token) {
            return false;
        }
        localStorage.setItem('auth_token', token);

        const profile = await Meteor.callPromise('api.getUserData', token);
        console.log("profile", profile);
        return dispatch({
            type: 'SET_INFO',
            payload: profile
        });
    }
}

function listUsers(data) {  
    return async function(dispatch) {
        const token = localStorage.getItem('auth_token');

        let result = [];
        if (token) {
            result = await Meteor.callPromise('api.listUsers', { token, filter: data });
        }
        return result;
    }
}

function getTransactions() {  
    return async function(dispatch) {
        console.log('called getTransactions');
        const token = localStorage.getItem('auth_token');

        if (!token) {
            console.log('no token');
            return false;
        }

        const txData = await Meteor.callPromise('api.listTransactions', { token });
        console.log(txData);
        return dispatch({
            type: 'SET_TRANSACTIONS',
            payload: txData,
        });
    }
}

function newTransaction(user, amount) {  
    return async function(dispatch) {
        const token = localStorage.getItem('auth_token');
        if (!token) {
            return false;
        }

        const txData = await Meteor.callPromise('api.newTransaction', { user, amount, token });
        if (!txData) {
            return false;
        }
        
        return dispatch({
            type: 'UPDATE_BALANCE',
            payload: txData,
        });
    }
}

function showLoader() {
    return async function(dispatch) {
        return dispatch({
            type: 'SHOW_LOADER',
            payload: true,
        });
    }
}

function hideLoader() {
    return async function(dispatch) {
        return dispatch({
            type: 'HIDE_LOADER',
            payload: false,
        });
    }
}

function suggestOnChange(val) {
    return async function(dispatch) {
        let suggestions = [];
        if (val.length > 0) {
            const token = localStorage.getItem('auth_token');
            suggestions = await Meteor.callPromise('api.listUsers', { token, filter: val }) || [];
        }
        console.log('got sugg: ', suggestions);
        return dispatch({
            type: 'SET_SUGGESTIONS',
            payload: suggestions,
        });
    }
}

function suggestSelected(val) {
    return async function(dispatch) {
        return dispatch({
            type: 'SET_SELECTED',
            payload: val,
        });
    }
}

function showError(val) {
    return async function(dispatch) {
        return dispatch({
            type: 'SET_ERROR',
            payload: val,
        });
    }
}
export default {  
  getProfile,
  getTransactions,
  newTransaction,
  suggestOnChange,
  suggestSelected,
  login,
  logout,
  register,
  showLoader,
  hideLoader,
  showError,
}
