import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';
import validator from 'email-validator';

class AuthPage extends React.Component {

    onLogin(e) {
        e.preventDefault();        
        const login = this.refs.uname.value;
        const passwd = this.refs.pass.value;

        if (!login || !passwd) {
            $('#loginErrorBox').html('<b>Missing email or password!</b>');
            return false;
        }

        if (!validator.validate(login)) {
            $('#loginErrorBox').html('<b>Please enter correct email!</b>');
            return false;
        }

        setTimeout(() => {
            $('#loginErrorBox').html('<b>Wrong email or password! Please try agan</b>');
        }, 3000);

        this.props.onLogin({ login, passwd });
        return true;
    }

    onLoginHeader(e) {
        e.preventDefault();
        this.$("#login-form").delay(100).fadeIn(100);
        this.$("#register-form").fadeOut(100);
        this.$('#register-form-link').removeClass('active');
        this.$(e.target).addClass('active');
    }

    onRegHeader(e) {
        e.preventDefault();
        this.$("#register-form").delay(100).fadeIn(100);
        this.$("#login-form").fadeOut(100);
        this.$('#login-form-link').removeClass('active');
        this.$(e.target).addClass('active');
    }

    onRegister(e) {
        e.preventDefault();
        const user = this.refs.newUser.value;
        const passwd = this.refs.newPass.value;
        const mail = this.refs.newMail.value;
        const pass2 = this.refs.newPass2.value;

        if (!user || !mail) {
            $('#errorbox').html('<b>User name or email is missing </b>');
            return false;
        }

        if (!validator.validate(mail)) {
            $('#errorbox').html('<b>Please enter valid email address </b>');
            return false;
        }

        if (!passwd || !pass2 || passwd != pass2) {
            $('#errorbox').html('<b>Passwords mismatch! </b>');
            return false;
        }

        if (passwd.length < 6) {
            $('#errorbox').html('<b>Password is too short! Min 6 characters </b>');
            return false;
        }

        this.props.onRegister({ user, mail, passwd });
        return false;
    }

    render() {
        return (
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-login">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="#" class="active" onClick={this.onLoginHeader} id="login-form-link">Login</a>
                    </div>
                    <div class="col-xs-6">
                        <a href="#" id="register-form-link" onClick={this.onRegHeader}>Register</a>
                    </div>
                </div>
                <hr />
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="login-form" method="post" style={{ display: 'block' }}>
                            <div class="form-group">
                                <input type="text" name="user" id="uname" class="form-control" ref="uname" tabIndex="1" placeholder="Email" />
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" ref="pass" id="pass" tabIndex="2" placeholder="Password" />
                            </div>
                            <div class="form-group">
                                <div id="loginErrorBox">&nbsp;</div>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <input onClick={this.onLogin.bind(this)} type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In" />
                                    </div>
                                </div>
                            </div>

                        </form>
                        <form id="register-form" method="post" role="form" style={{ display: 'none' }}>
                            <div class="form-group">
                                <input type="text" name="username" ref="newUser" id="username" tabindex="1" class="form-control" placeholder="Username" />
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" ref="newMail" id="email" tabIndex="1" class="form-control" placeholder="Email Address"/>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" ref="newPass" id="password" tabIndex="2" class="form-control" placeholder="Password" />
                            </div>
                            <div class="form-group">
                                <input type="password" name="confirm-password" ref="newPass2" id="confirm-password" tabIndex="2" class="form-control" placeholder="Confirm Password" />
                            </div>
                            <div class="form-group">
                                <div id="errorbox">&nbsp;</div>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <input onClick={this.onRegister.bind(this)} type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
    }
}

AuthPage.displayName = 'Auth';
AuthPage.propTypes = {
    onLogin: PropTypes.func,
    onRegister: PropTypes.func,
};

export default AuthPage;
