function transactions(state = [], action) {  
    switch (action.type) {
        case 'GET_TRANSACTIONS':
            return state;
        case 'ADD_TRANSACTION':
            console.log(action.payload);
            return [
                action.payload,
                ...state,
            ];
        case 'SET_TRANSACTIONS':
            // const { email, amount } = action.payload;
            return action.payload;
        default:
            return state
    }
}

export default transactions;
