function suggestVal(state = '', action) {  
    switch (action.type) {
        case 'GET_SELECTED':
            return state;
        case 'SET_SELECTED':
            return action.payload;
        default:
            return state
    }
}

export default suggestVal;
