function suggestions(state = [], action) {  
    switch (action.type) {
        case 'GET_SUGGESTIONS':
            console.log(state);
            return state;

        case 'SET_SUGGESTIONS':
            console.log('called SET_SUGGESTIONS' , action);
            if (!action.payload) {
                return [];
            }

            return action.payload;
        default:
            return state
    }
}

export default suggestions;
