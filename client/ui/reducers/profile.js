function profile(state = {}, action) {  
    switch (action.type) {
        case 'GET_INFO':
            console.log(state);
            return state;
        case 'UPDATE_BALANCE':
            console.log('CALLED NOW UPDATE_BALANCE ',action.payload);
            const newBalance = state.balance + action.payload.amount;
            return {
                ...state,
                balance: newBalance
            };
        case 'SET_INFO':
            if (!action.payload) {
                return {};
            }
            return action.payload;
        default:
            return state
    }
}

export default profile;
