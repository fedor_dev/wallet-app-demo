import { combineReducers } from 'redux'  
import transactions from './transactions'
import profile from './profile'
import suggestions from './suggestions'
import suggestVal from './suggestVal'
import payError from './payError'

const appReducer = combineReducers({  
  transactions,
  suggestions,
  suggestVal,
  payError,
  profile,
});

export default appReducer
