function payError(state = '', action) {  
    switch (action.type) {
        case 'GET_ERROR':
            return state;
        case 'SET_ERROR':
            return action.payload;
        default:
            return state
    }
}

export default payError;
