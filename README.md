# Meteor.js wallet app demo

Just demo of wallet, using external API. React + redux on frontend.  
Can be linked with any blockchain network (i.e. bitcoin network via https://bitaps.com/api/)  

![Screen](https://gitlab.com/fedor_dev/wallet-app-demo/raw/master/Screen_Shot_2019-05-06_at_12.12.54.png)
![Screen1](https://gitlab.com/fedor_dev/wallet-app-demo/raw/master/Screen_Shot_2019-05-06_at_12.14.33.png)

# How to run:

```
npm i
meteor run
```

Meteor app will be started on local port 3000.